package pl.kku.cats_kdk.dab.domain;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Cat {
	private Long id;
	private String name, ownerName;
	private Date dateBirthday;
	private Float weight;
	private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");

	/**
	 * @return the database id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the ownerName
	 */
	public String getOwnerName() {
		return ownerName;
	}

	/**
	 * @param ownerName
	 *            the ownerName to set
	 */
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	/**
	 * @return the dateBirthday
	 */
	public Date getDateBirthday() {
		return dateBirthday;
	}

	/**
	 * @param dateBirthday
	 *            the dateBirthday to set
	 */
	public void setDateBirthday(Date dateBirthday) {
		this.dateBirthday = dateBirthday;
	}

	/**
	 * @return the weight
	 */
	public Float getWeight() {
		return weight;
	}

	/**
	 * @param weight
	 *            the weight to set
	 */
	public void setWeight(Float weight) {
		this.weight = weight;
	}

	@Override
	public String toString() {
		return " Name: " + this.getName()
				+ "\n Owner: " + this.getOwnerName()
				+ "\n DateBirthday: " + (this.getDateBirthday()==null?"null":sdf.format(this.getDateBirthday()))
				+ "\n Weight: " + this.getWeight();
	}
}
