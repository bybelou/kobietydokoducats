package pl.kku.cats_kdk.dab.interfaces;

import org.springframework.data.repository.CrudRepository;
import pl.kku.cats_kdk.dab.entities.Cats;

import java.util.Optional;

public interface CatUpdateInterface extends CrudRepository<Cats, Long> {
    @Override
    Optional<Cats> findById(Long aLong);
}
