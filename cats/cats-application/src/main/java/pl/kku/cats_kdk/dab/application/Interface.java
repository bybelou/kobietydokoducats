package pl.kku.cats_kdk.dab.application;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import pl.kku.cats_kdk.dab.domain.Cat;
import pl.kku.cats_kdk.dab.service.CatsService;

public class Interface {
	private static CatsService catsService = new CatsService();
	private static Scanner scanner = new Scanner(System.in);

	private static Pattern patternIntNumber = Pattern.compile("[0-9]+");
	private static Pattern patternFloatNumber = Pattern.compile("[0-9]+(.?[0-9]+)?");
	private static Pattern patternString = Pattern.compile("[a-zA-Z]{2,}");
	private static Pattern patternDate = Pattern.compile("[0-9]{4}.[0-9]{2}.[0-9]{2}");
	private static SimpleDateFormat SDF = new SimpleDateFormat("yyyy.MM.dd");

	public static void main(String[] args) {
		// example ->>
		Cat cat = new Cat();
		cat.setName("Cat_Name");
		cat.setOwnerName("Owner_Cat_Name");
		try {
			cat.setDateBirthday(SDF.parse("2019.01.01"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		cat.setWeight(1.3f);
		catsService.addCat(cat);
		// example -<<

		String userChoose;
		do {
			System.out.println("xxxxxxxxxxxxxxxxxx");
			System.out.println("x      MENU      x");
			System.out.println("xxxxxxxxxxxxxxxxxx");
			System.out.println("1) Add cat");
			System.out.println("2) Show cat");
			System.out.println("x) Exit");

			userChoose = readUserInput();

			switch (userChoose) {
			case "1":
				addCat();
				break;
			case "2":
				showCats();
				break;
			case "x":
				continue;
			default:
				System.out.println("Bad choose, try again");
			}

		} while (!userChoose.equalsIgnoreCase("x"));
	}

	public static CatsService getCatsDAO() {
		return catsService;
	}

	public static void setCatsDAO(CatsService catsService) {
		Interface.catsService = catsService;
	}

	private static String readUserInput() {
		return scanner.nextLine().trim();
	}

	private static void showCats() {
		System.out.println("Cats list:");
		for (int i = 0; i < catsService.getCats().size(); i++) {
			System.out.println(i + 1 + ") " + catsService.getCats().get(i).getName());
		}

		System.out.println("Show all information of cat id:");

		Cat cat;
		boolean test;
		String readed;
		Matcher matcher;
		do {
			test = false;
			cat = null;

			readed = readUserInput();
			matcher = patternIntNumber.matcher(readed);

			if (matcher.matches())
				try {
					if (matcher.matches())
						//cat = catsDAO.getCat(Integer.parseInt(readed) - 1);
						cat = catsService.getCat(Long.parseLong(readed) - 1);
					if (cat == null)
						test = false;
					else {
						test = true;
						System.out.println(cat);
					}
				} catch (Exception e) {
					test = false;
				}

			if (!test)
				System.out.println("Please try again:");
		} while (!test);
	}

	private static void addCat() {
		String readed;
		Matcher matcher;
		Cat cat = new Cat();

		do {
			System.out.println("Set name: ");
			readed = readUserInput();
			matcher = patternString.matcher(readed);
		} while (!matcher.matches());
		cat.setName(readed);

		do {
			System.out.println("Set owner: ");
			readed = readUserInput();
			matcher = patternString.matcher(readed);
		} while (!matcher.matches());
		cat.setOwnerName(readed);

		boolean test;
		do {
			System.out.println("Set birthday: [format YYYY.MM.DD]");
			readed = readUserInput();
			matcher = patternDate.matcher(readed);
			test = false;
			if (matcher.matches())
				try {
					cat.setDateBirthday(SDF.parse(readed));
					test = true;
				} catch (ParseException e) {
					e.printStackTrace();
				}
			//else test = false;
		} while (!test);

		do {
			System.out.println("Set weight: ");
			readed = readUserInput();
			matcher = patternFloatNumber.matcher(readed);
		} while (!matcher.matches());
		cat.setWeight(Float.parseFloat(readed));

		catsService.addCat(cat);

		System.out.println("Added Cat: \n" + cat);
	}
}
