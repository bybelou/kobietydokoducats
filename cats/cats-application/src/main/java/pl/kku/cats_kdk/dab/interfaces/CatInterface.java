package pl.kku.cats_kdk.dab.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.kku.cats_kdk.dab.entities.Cats;

import java.util.List;
import java.util.Optional;

@Repository //to nie jest konieczna adnotacja
public interface CatInterface extends JpaRepository<Cats, Long> {
    Optional<Cats> findById(Long id);
    List<Cats> findAll();
}
