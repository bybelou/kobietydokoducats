package pl.kku.cats_kdk.dab.entities;

import pl.kku.cats_kdk.dab.domain.Cat;

import javax.persistence.*;
import java.util.Date;

@Entity//gdyby nazwa tabeli była inna niż klasy, to trzeba byłoby zastosować dodatkowo: @Table(name="cats")
@Table(name="cats")//zdaje się, że mysql rozpoznaje wielkość liter
public class Cats {
	@Id
	@Column(name="cat_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@Column(nullable = false)//nazwa kolumny identyczna z nazwą w tabeli, ale domyślnie adnotacja umożliwia wpisanie null
	private String name;

	@Column(name="owner_name", nullable = false)
	private String ownerName;

	@Column(name="date_birthday")
	private Date dateBirthday;

	@Column
	private Float weight;

	/**
	 * @return the database id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the ownerName
	 */
	public String getOwnerName() {
		return ownerName;
	}

	/**
	 * @param ownerName
	 *            the ownerName to set
	 */
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	/**
	 * @return the dateBirthday
	 */
	public Date getDateBirthday() {
		return dateBirthday;
	}

	/**
	 * @param dateBirthday
	 *            the dateBirthday to set
	 */
	public void setDateBirthday(Date dateBirthday) {
		this.dateBirthday = dateBirthday;
	}

	/**
	 * @return the weight
	 */
	public Float getWeight() {
		return weight;
	}

	/**
	 * @param weight
	 *            the weight to set
	 */
	public void setWeight(Float weight) {
		this.weight = weight;
	}

	public Cat getCat(){
		Cat cat = new Cat();
		cat.setId(this.getId());
		cat.setName(this.getName());
		cat.setWeight(this.getWeight());
		cat.setDateBirthday(this.getDateBirthday());
		cat.setOwnerName(this.getOwnerName());

		return cat;
	}
}
