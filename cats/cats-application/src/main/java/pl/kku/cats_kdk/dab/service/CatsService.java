package pl.kku.cats_kdk.dab.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.kku.cats_kdk.dab.domain.Cat;
import pl.kku.cats_kdk.dab.entities.Cats;
import pl.kku.cats_kdk.dab.interfaces.CatInterface;
import pl.kku.cats_kdk.dab.interfaces.CatUpdateInterface;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/*create table cats (cat_id int(11) auto_increment primary key,
    name varchar(100) not null,
    date_birthday date,
    weight float,
    owner_name varchar(100),
    constraint chk_cats_weight check (weigh>0));*/

/*
Róznica pomiędzy Statement a PreparedStatement
Zapytania do bazy danych możemy wykonywać na dwa sposoby:

wywołując metodę Connection.createStatement() a następnie metodę Statement.execute(String sql)
wywołując metodę Connection.prepareStatement(String sql) a następnie metodę PreparedStatement.execute()
W przypadku zapytań, które zwracają dane (np. zapytania typu Select) druga metoda ma nazwę executeQuery (typ zwracany to ResultSet zamiast boolean)

Jak sama widzisz, różnica jest w tym, kiedy podajemy samo zapytanie SQL (na końcu vs początku).
Jest to spowodowane tym, że PreparedStatement wstępnie kompiluje zapytanie SQL skracając czas jego wykonywania — jest to bardzo korzystne,
kiedy wykonujemy podobne zapytanie wiele razy (np. w pętli) zmieniając jedynie pewne parametry. Dodatkową korzyścią jest to,
że PreparedStatement zabezpiecza nas przed atakami typu SQL injection (dzięki temu, że możemy używać parametrów — w przypadku zapytań typu Statement sami musimy o to zadbać
i wstawić je do zapytania ręcznie, konstruując String’a). Dobrą praktyką jest korzystanie właśnie z PreparedStatement, chyba, że nie jest to możliwe
(pewne elementy zapytań SQL wymagają użycia Springowej klasy NamedParameterJdbcTemplate jesli chcemy uniknąć ręcznego konstruowania zapytań SQL).*/

@Service
public class CatsService {
    public CatsService() {// orignal constructor
    }

    //SpringData, można też korzystać z JPA i EntityManagera
    @Autowired
    protected CatInterface catInterface;
    @Autowired
    protected CatUpdateInterface catUpdateInterface;

    public Cat getCat(Long idCat) {
        Optional<Cats> opt = catInterface.findById(idCat);

        return opt.map(Cats::getCat).orElse(null);
    }

    public List<Cat> getCats(){
        //Optional<List<Cats>> opt = catInterface.getCats();
        List<Cats> list = catInterface.findAll();
        if (!list.isEmpty()) {
            return list.stream().map(Cats::getCat).collect(Collectors.toList());
        } else {
            return null;
        }
    }

    public void addCat(Cat cat) {
        Cats cats = new Cats();
        cats.setName(cat.getName());
        cats.setDateBirthday(cat.getDateBirthday());
        cats.setWeight(cat.getWeight());
        cats.setOwnerName(cat.getOwnerName());

        catInterface.save(cats);
    }



    //JPA
    /*
    Dodanie pola typu EntityManager oraz dodając nad nim adnotację @PersistenceContext to wszystko, co musimy zrobić aby móc używać go w naszej aplikacji. Dodatkowo, każda metoda, która korzysta z EntityManagera msui mieć adnotację @Transactional — jest to ważne, ponieważ pozwala nam to "powiedzieć"’" bibliotece Hibernate kiedy jest początek i kiedy koniec transakcji, dzięki czemu nie będziemy mieli problemów z tym, że pewne zmiany nie będą widoczne lub będą powodowały problemy.*/
    //https://docs.oracle.com/javaee/6/api/javax/persistence/EntityManager.html - dokumentacja entityManagera
    /*@PersistenceContext
    EntityManager entityManager;*/

    /*merge vs persist
    Różnica pomiędzy tymi metodami jest taka, że metoda merge zaktualizuje obiekt w bazie danych, jeśli ma takie samo id. Najczęściej tworząc nowe obiekty, nie przypisujemy im id (pozwalamy, żeby baza danych je sama utworzyła). W takiej sytuacji można bezpiecznie używać merge zarówno do tworzenia jak i aktualizacji obiektów. Jeśli jednak chcemy być w 100% poprawni, powinniśmy użyć metody persist.
    Różnica jest też w specyficznym zachowaniu dot. zarządzania encją (patrz wyżej) — merge tworzy kopię obiektu, i to ta kopia jest zarządzana. Weźmy wiec pod uwagę następującą sytuacje:*/
    /*@Transactional
    public void addCat(Cat cat) {
        Cats cats = new Cats();
        cats.setName(cat.getName());
        cats.setDateBirthday(cat.getDateBirthday());
        cats.setWeight(cat.getWeight());
        cats.setOwnerName(cat.getOwnerName());

        cats = entityManager.merge(cats);
    }*/
    /*public Cat getCat(Long idCat) {
        return entityManager.find(Cats.class, idCat).getCat();
    }*/

    /*public List<Cat> getCats() {
        Query query = entityManager.createQuery("from Cats c", Cats.class);
        List<Cats> catsList = query.getResultList();
        //List<Cat> catList = catsList.stream().map(Cats::getCat).collect(Collectors.toList());

        return catsList.stream().map(Cats::getCat).collect(Collectors.toList());
    }*/





    //wykorzystując bazę danych za pomocą JDBC
    /*
    @Autowired
    private DataSource dataSource;

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    private void databasePrepareStatement(String sql) throws SQLException {
        if (ps != null && !ps.isClosed()) ps.close();
        if (rs != null && !rs.isClosed()) rs.close();
        if (conn != null && !conn.isClosed()) conn.close();

        conn = dataSource.getConnection();
        ps = conn.prepareStatement(sql);
    }

    private void runPreparedStatement(String opt) throws SQLException {
        switch(opt){
            case "U": {
                ps.executeUpdate();
                break;
            } case "S": {
                rs = ps.executeQuery();
                break;
            }
        }
        //executeQuery - select
        //executeUpdate - insert,update,delete
        //execute - wszystko
    }

    private void closeConnections() {
        try {
            if (ps != null && !ps.isClosed()) ps.close();
            if (rs != null && !rs.isClosed()) rs.close();
            if (conn != null && !conn.isClosed()) conn.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void addCat(Cat cat) {
        String sql = "insert into cats (name,date_birthday,weight,owner_name) values (?,?,?,?)";

        try {
            databasePrepareStatement(sql);

            ps.setString(1, cat.getName());
            //Optional.ofNullable(null).ifPresent(ps::setDate(2, new java.sql.Date(cat.getDateBirthday().getTime())));
            ps.setDate(2, Optional.ofNullable(cat.getDateBirthday()).map(o -> new java.sql.Date(o.getTime())).orElse(null));
            if ((cat.getWeight() == null))
                ps.setNull(3, Types.FLOAT);
            else
                ps.setFloat(3, cat.getWeight());

            ps.setString(4, cat.getOwnerName());
            runPreparedStatement("U");

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            closeConnections();
        }
    }

    public Cat getCat(int idCat) {
        String sql = "select * from cats where cat_id = ?";

        Cat cat = null;

        try {

            databasePrepareStatement(sql);
            ps.setInt(1, idCat);
            runPreparedStatement("S");

            if (rs.next()) {
                cat = new Cat();
                cat.setId(rs.getInt("cat_id"));
                cat.setName(rs.getString("name"));
                cat.setDateBirthday(rs.getDate("date_birthday"));
                cat.setWeight(rs.getFloat("weight"));
                cat.setOwnerName(rs.getString("owner_name"));
            }

            return cat;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            closeConnections();
        }
    }

    public List<Cat> getCats() {
        String sql = "select * from cats";

        List<Cat> catList = new ArrayList<>();

        try {

            databasePrepareStatement(sql);
            runPreparedStatement("S");

            while (rs.next()) {
                Cat cat = new Cat();
                cat.setId(rs.getInt("cat_id"));
                cat.setName(rs.getString("name"));
                cat.setDateBirthday(rs.getDate("date_birthday"));
                cat.setWeight(rs.getFloat("weight"));
                cat.setOwnerName(rs.getString("owner_name"));

                catList.add(cat);
            }

            return catList;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            closeConnections();
        }
    }*/





    //wykorzystując listę zamiast bazy danych
    /*
    List<Cat> catsList = new ArrayList<Cat>();

    public void addCat(Cat cat) {
        this.catsList.add(cat);
    }

    public Cat getCat(int idCat) {
        return this.catsList.get(idCat);
    }

    public List<Cat> getCats() {
        return this.catsList;
    }*/
}
