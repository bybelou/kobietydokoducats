package pl.kku.cats_kdk.dab.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.kku.cats_kdk.dab.service.CatsService;
import pl.kku.cats_kdk.dab.domain.Cat;
import pl.kku.cats_kdk.dab.dto.FormularzKotaDodajPoprawnyDTO;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.text.ParseException;
import java.text.SimpleDateFormat;

@Controller
public class CatsController {
    @Autowired
    private CatsService catsService;

    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");

    @RequestMapping("")
    public String metoda(Model model) {
        model.addAttribute("message", "<B>Przekazany string:</B><BR/>"
                + "@ page language=\"java\" contentType=\"text/html; charset=\"pageEncoding=\"ISO-8859-2\"%>");

        String[] tab = {"pierwszy", "drugi", "trzeci", "czwarty"};
        model.addAttribute("lista", tab);

        model.addAttribute("pensja", 2123);

        return "glowna";
    }

    @RequestMapping("/lista")
    public String catsList(Model model) {
        model.addAttribute("cats", catsService.getCats());

        return "lista";
    }

    @RequestMapping("/kot-{id}")
    public String catDetails(@PathVariable("id") Long idCat, Model model) {
        model.addAttribute("cat", catsService.getCat(idCat));

        return "szczegoly";
    }

    @RequestMapping("/dodajProsty")
    public String addCatSimple(Model model, HttpServletRequest request,
                               @RequestParam(value = "imie", required = false, defaultValue = "") String imie,
                               @RequestParam(value = "dataUrodzenia", required = false, defaultValue = "") String dataUrodzenia,
                               @RequestParam(value = "waga", required = false, defaultValue = "") String waga,
                               @RequestParam(value = "imieOpiekuna", required = false, defaultValue = "") String imieOpiekuna) {
        if (request.getMethod().equalsIgnoreCase("get"))
            return "dodajProsty";
        else {
            Cat cat = new Cat();
            try {
                if (dataUrodzenia!=null)
                    cat.setDateBirthday(sdf.parse(dataUrodzenia));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            cat.setName(imie);
            cat.setOwnerName(imieOpiekuna);
            if (waga!=null && waga.equals(""))
                cat.setWeight(Float.parseFloat(waga.replace(",", ".")));

            catsService.addCat(cat);

            return "redirect:lista";
        }
    }

    @RequestMapping("/dodajKobietyDoKodu")
    public String addCatKobietyDoKodu(HttpServletRequest request, @ModelAttribute("formularzKotaDodajPoprawnyDTO") @Valid FormularzKotaDodajPoprawnyDTO formularzKotaDodajPoprawnyDTO, BindingResult result) {
        if (request.getMethod().equalsIgnoreCase("post") && !result.hasErrors()) {
            Cat cat = new Cat();
            cat.setDateBirthday(formularzKotaDodajPoprawnyDTO.getDataUrodzenia());
            cat.setName(formularzKotaDodajPoprawnyDTO.getImie());
            cat.setOwnerName(formularzKotaDodajPoprawnyDTO.getImieOpiekuna());
            cat.setWeight(formularzKotaDodajPoprawnyDTO.getWaga());
            catsService.addCat(cat);

            return "redirect:/lista";
        }
        return "dodajKobietyDoKodu";
    }


    @ModelAttribute("formularzKotaDodajPoprawnyDTO")
    public FormularzKotaDodajPoprawnyDTO getFormularz() {
        return new FormularzKotaDodajPoprawnyDTO();
    }

    @RequestMapping(value = "/dodajPoprawny", method = RequestMethod.GET)
    public String formularzKotaDodajPoprawny() {
        return "/dodajPoprawny";
    }

    @RequestMapping(value = "/dodajPoprawny", method = RequestMethod.POST)
    public String obsluzFormularzKotaDodajPoprawny(@ModelAttribute("formularzKotaDodajPoprawnyDTO") @Valid FormularzKotaDodajPoprawnyDTO formularzKotaDodajPoprawnyDTO, BindingResult result) {
        if (result.hasErrors()) { //formularz nie jest uzupełniony prawidłowo
            return "/dodajPoprawny";
        } else { //formularz wypełniony prawidłowo
            Cat cat = new Cat();
            cat.setDateBirthday(formularzKotaDodajPoprawnyDTO.getDataUrodzenia());
            cat.setName(formularzKotaDodajPoprawnyDTO.getImie());
            cat.setOwnerName(formularzKotaDodajPoprawnyDTO.getImieOpiekuna());
            cat.setWeight(formularzKotaDodajPoprawnyDTO.getWaga());
            catsService.addCat(cat);

            return "redirect:/lista"; //glowna
        }
    }

    /*
    Na początku jak tworzysz pusty formularz to musisz też utworzyć pusty obiekt form w metodzie GET,
    bo teraz tagi form szukają obiektu form którego nie znajdują:

    W CatsController musisz zmodyfikowac metodę

    Możesz to zrobić na trzy sposoby

    Czyli tworzysz widok i wypełniasz go modelem z obiektami które mają być widoczne w widoku jsp
    (tutaj mamy jedynie „form”, ale możesz też dodawać inne)
    */
    @RequestMapping(value = "/dodajPoprawnyMetoda1", method = RequestMethod.GET)
    public ModelAndView addCatPropertyMethod1(ModelMap model) {
        model.addAttribute("formularzKotaDodajPoprawnyDTO", new FormularzKotaDodajPoprawnyDTO());
        System.out.println("dodajPoprawnyMetoda1 - A");
        return new ModelAndView("dodajPoprawnyMetoda1", model);
    }

    @RequestMapping(value = "/dodajPoprawnyMetoda1", method = RequestMethod.POST)
    public String addCatPropertyServiceMethod1(@ModelAttribute("formularzKotaDodajPoprawnyDTO") @Valid FormularzKotaDodajPoprawnyDTO formularzKotaDodajPoprawnyDTO, BindingResult result) {
        if (result.hasErrors()) {
            //formularz nie jest prawidłowo uzupełniony
            System.out.println("dodajPoprawnyMetoda1 - B");
            return "/dodajPoprawnyMetoda1";
        } else {
            //formularz wypełniony prawidłowo
            Cat cat = new Cat();
            cat.setDateBirthday(formularzKotaDodajPoprawnyDTO.getDataUrodzenia());
            cat.setName(formularzKotaDodajPoprawnyDTO.getImie());
            cat.setOwnerName(formularzKotaDodajPoprawnyDTO.getImieOpiekuna());
            cat.setWeight(formularzKotaDodajPoprawnyDTO.getWaga());
            catsService.addCat(cat);

            System.out.println("dodajPoprawnyMetoda1 - C");
            return "redirect:/lista";
        }
    }


    /*
    Można tę metodę skrócić i skorzystać z bardziej nowoczesnego springa:
    I mamy to samo co w pierwszym sposobie ale krócej trochę.
     */
    @RequestMapping(value = "/dodajPoprawnyMetoda2", method = RequestMethod.GET)
    public String addCatPropertyMethod2(ModelMap model) {
        model.addAttribute("formularzKotaDodajPoprawnyDTO", new FormularzKotaDodajPoprawnyDTO());
        System.out.println("dodajPoprawnyMetoda2 - A");
        return "dodajPoprawnyMetoda2";
    }

    @RequestMapping(value = "/dodajPoprawnyMetoda2", method = RequestMethod.POST)
    public String addCatPropertyServiceMethod2(@ModelAttribute("formularzKotaDodajPoprawnyDTO") @Valid FormularzKotaDodajPoprawnyDTO formularzKotaDodajPoprawnyDTO, BindingResult result) {
        if (result.hasErrors()) {
            //formularz nie jest prawidłowo uzupełniony
            System.out.println("dodajPoprawnyMetoda2 - B");
            return "/dodajPoprawnyMetoda2";
        } else {
            //formularz wypełniony prawidłowo
            Cat cat = new Cat();
            cat.setDateBirthday(formularzKotaDodajPoprawnyDTO.getDataUrodzenia());
            cat.setName(formularzKotaDodajPoprawnyDTO.getImie());
            cat.setOwnerName(formularzKotaDodajPoprawnyDTO.getImieOpiekuna());
            cat.setWeight(formularzKotaDodajPoprawnyDTO.getWaga());
            catsService.addCat(cat);

            System.out.println("dodajPoprawnyMetoda2 - C");
            return "redirect:/lista";
        }
    }


    /*
    Można to też zupełnie uproscić i skorzystać z adnotacji:
    Trzeci sposób robi to samo co pierwszy i drugi a właściwie to adnotracje odwalają za nas robotę
    (pod spodem we wnętrznościach springa dzieje się dokładnie to co w pierwszym sposobie)
     */
    @RequestMapping(value = "/dodajPoprawnyMetoda3", method = RequestMethod.GET)
    public String addCatPropertyMethod3(@ModelAttribute("formularzKotaDodajPoprawnyDTO") FormularzKotaDodajPoprawnyDTO formularzKotaDodajPoprawnyDTO) {
        System.out.println("dodajPoprawnyMetoda3 - A");
        return "dodajPoprawnyMetoda3";
    }

    @RequestMapping(value = "/dodajPoprawnyMetoda3", method = RequestMethod.POST)
    public String addCatPropertyServiceMethod3(@ModelAttribute("formularzKotaDodajPoprawnyDTO") @Valid FormularzKotaDodajPoprawnyDTO formularzKotaDodajPoprawnyDTO, BindingResult result) {
        if (result.hasErrors()) {
            //formularz nie jest prawidłowo uzupełniony
            System.out.println("dodajPoprawnyMetoda3 - B");
            return "/dodajPoprawnyMetoda3";
        } else {
            //formularz wypełniony prawidłowo
            Cat cat = new Cat();
            cat.setDateBirthday(formularzKotaDodajPoprawnyDTO.getDataUrodzenia());
            cat.setName(formularzKotaDodajPoprawnyDTO.getImie());
            cat.setOwnerName(formularzKotaDodajPoprawnyDTO.getImieOpiekuna());
            cat.setWeight(formularzKotaDodajPoprawnyDTO.getWaga());
            catsService.addCat(cat);

            System.out.println("dodajPoprawnyMetoda3 - C");
            return "redirect:/lista";
        }
    }
}