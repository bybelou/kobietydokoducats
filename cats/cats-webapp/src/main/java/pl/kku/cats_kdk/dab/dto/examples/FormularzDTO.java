package pl.kku.cats_kdk.dab.dto.examples;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

public class FormularzDTO {
    @NotEmpty
    @Size(min = 3)
    private String imie;
    @NotEmpty
    @Email
    private String email;
    @Min(18)
    private Integer wiek;

    //gettery i settery
    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getWiek() {
        return wiek;
    }

    public void setWiek(Integer wiek) {
        this.wiek = wiek;
    }
}