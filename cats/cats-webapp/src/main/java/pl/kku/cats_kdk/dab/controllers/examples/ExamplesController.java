package pl.kku.cats_kdk.dab.controllers.examples;


import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import pl.kku.cats_kdk.dab.dto.examples.Employee;
import pl.kku.cats_kdk.dab.dto.examples.FormularzDTO;

import javax.validation.Valid;

@Controller
public class ExamplesController {
    /*
    Kontroler z zabezpieczeniami z kursu kobiety do kodu
    */
    @ModelAttribute("form")
    public FormularzDTO getFormularz() {
        return new FormularzDTO();
    }

    @RequestMapping(value = "/examples/formularz", method = RequestMethod.GET)
    public String formularz() {
        return "/examples/formularz";
    }

    @RequestMapping(value = "/examples/formularz", method = RequestMethod.POST)
    public String obsluzFormularz(@ModelAttribute("form") @Valid FormularzDTO form, BindingResult result) {
        if (result.hasErrors()) { //formularz nie jest uzupełniony prawidłowo
            return "/examples/formularz";
        } else { //formularz wypełniony prawidłowo
            return "redirect:/"; //glowna
        }
    }


    /*
    https://www.baeldung.com/spring-mvc-form-tutorial
    https://www.baeldung.com/spring-mvc-thymeleaf-data
    */
    @RequestMapping(value = "/examples/addEmployee", method = RequestMethod.GET)
    public ModelAndView showForm() {
        return new ModelAndView("/examples/addEmployee", "employee", new Employee());
    }

    @RequestMapping(value = "/examples/addEmployee", method = RequestMethod.POST)
    public String submit(@Valid @ModelAttribute("employee")Employee employee,
                         BindingResult result, ModelMap model) {
        if (result.hasErrors()) {
            System.out.println("error");
            //return "/examples/addEmployee";
            return "/examples/error";
        }
        model.addAttribute("name", employee.getName());
        model.addAttribute("contactNumber", employee.getContactNumber());
        model.addAttribute("id", employee.getId());
        System.out.println("caly obiekt: " + model);
        //return "redirect:/";//glowna
        return "/examples/employeeView";
    }
}