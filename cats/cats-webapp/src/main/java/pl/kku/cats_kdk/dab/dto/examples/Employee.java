package pl.kku.cats_kdk.dab.dto.examples;

import org.hibernate.validator.constraints.NotEmpty;

public class Employee {
    @NotEmpty
    private String name;
    private long id;
    private String contactNumber;

    // standard getters and setters

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }
}
