package pl.kku.cats_kdk.dab.dto;


import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Size;
import java.util.Date;

public class FormularzKotaDodajPoprawnyDTO {
	/*@NotEmpty
	@Email
	@Size(min=3)
	@Min(18)
	@Range(min=x, max=y) — sprawdza czy wartość jest z przedziału x, y
	@Size(min=x, max=y) — sprawdza długość ciągu znaków
	@DateTimeFormat(pattern=“MM/dd/yyyy”) — opisuje format daty, jaki chcemy akceptować
	@Past — sprawdza, czy data jest w przeszłości


	Cześć,
	mam pytanie. Co można zrobić, aby te błędy wyświetlały się po Polsku a nie po Angielsku?
	Pozdrawiam

	Cześć,
	najprostszy sposób to ustawienie pola message dla adnotacji walidujących, np:
	@NotNull(message="To pole nie może być puste") String email;

	Minusem jest to, że komunikaty będą tylko w języku polskim. 'Prawidłowy' sposób, który pozwala na obsługę wielu języków jest poprzez bundle. Po pierwsze musisz w swojej aplikacji ustawić domyślny język na język polski, np poprzez taką konfigurację XML:
	<bean id="localeResolver" class="org.springframework.web.servlet.i18n.SessionLocaleResolver">
	<property name="defaultLocale" value="pl"/>
	</bean>

	Po drugie musisz przetłumaczyć komunikaty - ponieważ spring korzysta ze standardowego API do walidacji w Javie, wpisz w google "jsr303 i18n" albo "jsr303 localization", kilka przykładowych stron:
	http://www.codebulb.ch/2015... (nieco chaotyczna, ale z przykładami)
	http://stackoverflow.com/qu... (nie ma znaczenia, że dotyczy JSF - walidacja i tłumaczenia są częścią Javy i zarówno Spring jak i JSF korzystają z nich identycznie)
	*/

	@NotEmpty(message="To pole nie może być puste")
	@Size(min=2)
	private String imie;
	//@NotEmpty
	@DateTimeFormat(pattern="yyyy.MM.dd")
	//@Pattern(regexp="[0-3]?[0-9]\\.[0-1]?[0-9]\\.[1-2][0-9]{3}") //może też tak być
	private Date dataUrodzenia;
	//@NotEmpty
	private Float waga;
	@Size(min=2)
	private String imieOpiekuna;

	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public Date getDataUrodzenia() {
		return dataUrodzenia;
	}

	public void setDataUrodzenia(Date dataUrodzenia) {
		this.dataUrodzenia = dataUrodzenia;
	}

	public Float getWaga() {
		return waga;
	}

	public void setWaga(Float waga) {
		this.waga = waga;
	}

	public String getImieOpiekuna() {
		return imieOpiekuna;
	}

	public void setImieOpiekuna(String imieOpiekuna) {
		this.imieOpiekuna = imieOpiekuna;
	}
}