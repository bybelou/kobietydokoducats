﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> <!-- TagLib do użycia w EL-->
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> <!-- TagLib do użycia w EL -->

<!-- W razie potrzeby EL można dezaktywować w ramach danego widoku — używamy do tego dyrektywy isELIgnored: -->
<%-- <%@ page isELIgnored="true" %> --%>
<%@ page isELIgnored="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>Aplikacja koty</title>
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
</head>
<body>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

	<B>Witaj, jesteś na stronie bazy danych kotów</B>

	<br /><br />

	<a href="lista">Lista kotów</a>
	<br />
	<a href="examples/addEmployee">Przykładowy formularz employee</a>
	<br />
	<a href="examples/formularz">Przykładowy formularz z kobiety do kodów z walidatorem - działa po dodaniu poniższego</a>
    <br />
    <b>Opis:</b><br />
    A z kolei ostatnie wersje formularza zadziałały dopiero po dodaniu<br />stworzenia instancji dla 'form'<br />tj.<br /><br />
    @ModelAttribute("form")<br />public FormularzDTO getFormularz() {<br />return new FormularzDTO();<br />}<br />
    <br />Wtedy wszystko działa po wykonaniu czynności opisanych w tej lekcji :)


	<br /><br />
	${message}
	<%-- <strong>${message}</strong> --%>
	
	<br /><br />
	<strong>ForEach: 1</strong><br /><hr />

	<%-- Pętla ta pozwala nam przechodzić po całej kolekcji elementów (podobnie jak uproszczona składnia pętli for), przy czym zbiorem elementów może być np . ciąg znaków (zbiór literek) lub zbiór liczb. Tag ten posiada kilka atrybutów, których możemy używać w zależności od sytuacji, najważniejsze z nich to:
    items — kolekcja obiektów, jeśli chcemy przejść przez wszystkie obiekty
    var — nazwa zmiennej, która będzie widoczna wewnątrz tego tagu i dostępna w ramach EL
    step — krok, czyli co ile elementów się przemieszczamy (domyślnie: 1 — idziemy do przodu po każdym elemencie, możemy też iść do tyłu, przypisując tej zmiennej wartość ‑1)
    begin — początkowy element (indeks elementu)
    end — końcowy element (indeks elementu) --%>
	<c:forEach var="iterator" begin="1" end="5">
		${iterator}, 
	</c:forEach>
	
	<br /><br />
	<strong>ForEach: 2</strong><br /><hr />
	<c:forEach var="iterator" items="${lista}">
		${iterator}<br />
	</c:forEach>
	
	<br /><br />
	<strong>ForEach: 3</strong><br /><hr />
	<c:forEach varStatus="status" var="iterator" items="${lista}">
		Element #${status.index}: ${iterator}<br />
	</c:forEach>
	
	<br /><br />

	PENSJA: ${pensja}<br />
	<strong>If: 1</strong><br /><hr />
	<c:if test="${pensja > 1000}">
		Zarabiam powyżej 1000 zł.
	</c:if>
	
	<br /><br />
	<strong>If: 2</strong><br /><hr />
	<c:if test="${pensja > 2000}">
		Zarabiam powyżej 2000 zł.
	</c:if>
	<c:if test="$!{pensja > 2000}">
		Zarabiam poniżej 2000 zł.
	</c:if>
	
	<br /><br />
	<strong>Choose</strong><br /><hr />
	<c:choose>
		<c:when test="$pensja < 2000">
			Zarabiasz mało
		</c:when>
		<c:when test="$pensja >= 19000">
			Zarabiasz dużo
		</c:when>
		<c:otherwise>
			Zarabiasz umiarkowanie
		</c:otherwise>
	</c:choose>
	
	<br /><br />
	<strong>Funkcja - lenght</strong><br /><hr />
	Długość kolekcji 'lista' - ${fn:length(lista)}
	
	<br /><br />
	<strong>Empty - null lub ciąg 0 znakow</strong><br /><hr />
	${empty(lista)}
	pensja: ${pensja} | length: ${fn:length(lista)}<br />
	pensja + length: ${pensja + fn:length(lista)}
	
</body>
</html>