<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="pl">
<head>
	<title>Lista kotów</title>
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
</head>
<body>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

	<a href="dodajProsty">Dodaj kota</a><br />
	<a href="dodajKobietyDoKodu">Dodaj kota KobietyDoKodu</a><br />
	<a href="dodajPoprawny">Dodaj kota w poprawnym formularzu</a><br />
	<a href="dodajPoprawnyMetoda1">Dodaj kota w poprawnym formularzu - metoda 1</a><br />
	<a href="dodajPoprawnyMetoda2">Dodaj kota w poprawnym formularzu - metoda 2</a><br />
	<a href="dodajPoprawnyMetoda3">Dodaj kota w poprawnym formularzu - metoda 3</a><br />
	<br />
	<br />
	tylko nie wstawiaj nazwy kota: < sc ript type="application/javascript">alert('bum');< /scr ipt> ;-)
	<br />
	<br />
	<table border="1">
		<thead>
			<tr>
				<th>#</th>
				<th>Id</th>
				<th>Imie kota</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach varStatus="status" var="element" items="${cats}">
				<tr>
					<td>${status.index + 1}</td>
					<td>${element.getId()}</td>
					<td><a href="kot-${element.getId()}">${element.getName()}</a>
				</td>
			</c:forEach>
		</tbody>
	</table>
	
	<br />
	<hr />
	
	Uwaga! W kontekście użycia HTML oraz sposobu budowania stron, te rozwiązania są bardziej antyprzykładem niż materiałem do nauki!
	<br />
	Jeśli chciałabyś także tworzyć same widoki i pisac poprawny, dobrze skonstruowany kod HTML, zachęcamy do zapoznania się np. z frameworkiem
	<a href="http://getbootstrap.com" target="_blank">Bootstrap</a>.
</body>
</html>