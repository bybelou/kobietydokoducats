<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> <!-- TagLib do użycia w EL -->
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Dodaj kota</title>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
</head>
<body>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

	<a href="lista">Powrót do listy kotów</a>
	<br />

	<form:form action="dodajPoprawnyMetoda1" modelAttribute="formularzKotaDodajPoprawnyDTO" method="POST">
	    <table border="1">
            <tbody>
	        <tr>
	            <th>
	                <form:label path="imie">Imię:</form:label>
	            </th>
	            <td>
	                <form:input path="imie" id="imie"></form:input>
	                <c:if test="${pageContext.request.method=='POST'}">
	                    <form:errors path="imie" />
	                </c:if>
	            </td>
	        </tr>
	        <tr>
	            <th>
	                <form:label path="dataUrodzenia">Data urodzenia:</form:label>
	            </th>
	            <td>
	                <form:input path="dataUrodzenia" id="dataUrodzenia"></form:input>
	                <c:if test="${pageContext.request.method=='POST'}">
	                    <form:errors path="dataUrodzenia" />
	                </c:if>
	            </td>
	        </tr>
	        <tr>
	            <th>
	                <form:label path="waga">Waga:</form:label>
	            </th>
	            <td>
	                <form:input path="waga" id="waga"></form:input>
	                <c:if test="${pageContext.request.method=='POST'}">
	                    <form:errors path="waga" />
	                </c:if>
	            </td>
	        </tr>
	        <tr>
	            <th>
	                <form:label path="imieOpiekuna">Imię opiekuna:</form:label>
	            </th>
	            <td>
	                <form:input path="imieOpiekuna" id="imieOpiekuna"></form:input>
	                <c:if test="${pageContext.request.method=='POST'}">
	                    <form:errors path="imieOpiekuna" />
	                </c:if>
	            </td>
	        </tr>
            <tr>
                <th>Zapisac?</th>
                <td><input type="submit" value="Submit"/></td>
            </tr>
	    </table>
	    </tbody>
	</form:form>

	<br />
	<hr />

	<!--Uwaga! W kontekście użycia HTML oraz sposobu budowania stron, te rozwiązania są bardziej antyprzykładem niż materiałem do nauki!<br />-->

	Jeśli chciałabyś także tworzyć same widoki i pisac poprawny, dobrze skonstruowany kod HTML, zachęcamy do zapoznania się np. z frameworkiem
	<a href="http://getbootstrap.com" target="_blank">Bootstrap</a>.
</body>
</html>