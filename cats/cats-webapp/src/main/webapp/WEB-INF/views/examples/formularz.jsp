<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Dodaj kota - Kobiety do Kodu</title>
	</head>
	<body>
		<a href="glowna">Powrót do strony głównej</a><br />
		<br />
		<form:form action="formularz" modelAttribute="form" method="post">
		    Imię:
		    <form:input path="imie" id="imie"></form:input>
		    <c:if test="${pageContext.request.method=='POST'}">
		        <form:errors path="imie" />
		    </c:if>
		    <br />
		    Adres email:
		    <form:input path="email" id="email"></form:input>
		    <c:if test="${pageContext.request.method=='POST'}">
		        <form:errors path="email" />
		    </c:if>
		    <br />
		    Wiek:
		    <form:input path="wiek" id="wiek"></form:input>
		    <c:if test="${pageContext.request.method=='POST'}">
		        <form:errors path="wiek" />
		    </c:if>
		    <br />
		    <input type="submit" value="Wyślij formularz" />
		</form:form>
		<br />
		<hr />
		Uwaga! W kontekście użycia HTML oraz sposobu budowania stron, te rozwiązania są bardziej antyprzykładem niż materiałem do nauki!<br />
		Jeśli chciałabyś także tworzyć same widoki i pisac poprawny, dobrze skonstruowany kod HTML, zachęcamy do zapoznania się np. z frameworkiem <a href="http://getbootstrap.com" target="_blank">Bootstrap</a>.
	</body>
</html>